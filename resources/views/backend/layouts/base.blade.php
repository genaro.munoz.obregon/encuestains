<!DOCTYPE html>
<html lang="en">
    {{-- HEAD --}}
    @include('backend.common.head')
    @if(config('configSite.vertical'))
    <body class="vertical-layout vertical-menu-modern  navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="">

        

        <!-- BEGIN: Content-->
        <div class="app-content content-fluid ">
            <div class="content-overlay"></div>
            <div class="content-wrapper container-xxl p-0">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <div id="app">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
        <!-- END: Content-->
          

        @include('backend.common.footer')


            @include('backend.common.scripts')


        </body>
@else
    
@endif

</html>
