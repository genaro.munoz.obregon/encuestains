<!DOCTYPE html>
<html lang="en">

    @include('backend.common.head')

    <body class="vertical-layout vertical-menu-modern blank-page navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="blank-page">
  
        <div class="app-content content ">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
    
            <div class="content-wrapper">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <div class="auth-wrapper auth-cover">
                        <div class="auth-inner row m-0">
                            
    
                            <!-- Left Text-->
                            <div class="d-none d-lg-flex col-lg-8 align-items-center p-5">
                                <div class="w-100 d-lg-flex align-items-center justify-content-center px-5">
                                    <img class="img-fluid" src="/backend/images/pages/login-v2.svg" alt="Login V2" /></div>
                            </div>
                            <!-- /Left Text-->
                            <!-- Login-->
                            <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
                                
                                <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                                    
                                    <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 text-center">
                                        <a class="brand-logo" href="/">
                                                <img src="{{config('configSite.logo_site')}}" class="logo-back" alt="My Business Card">
                                        
                                        </a>
                                    </div>
                                    <p class="card-text mb-2">Por favor ingrese su documento o correo electronico</p>
                                    <form action="{{ route('valida') }}"
                                        method="POST"
                                        id="formLogin">
                                        @csrf
                                        <div class="mb-1{{$errors->has('documento' ? 'is-invalid' : '')}}">
                                            <label class="form-label" for="documento">Documento o Correo electronico</label>
                                            <input class="form-control" value="{{old('documento')}}" type="text" required="" id="documento" name="documento" placeholder="documento o correo electronico">
                                        </div>
                                        
                                        <div class="form-group text-center m-t-20">
                                            <div class="col-12">
                                                
                                                {!!$errors->first('documento','<div class="row text-center pt-1"><span class="alert alert-danger btn-block btn-lg">:message</span></div>')!!}
                                            </div>
                                        </div>
                                        <button class="btn btn-primary w-100" tabindex="4">Consultar</button>
                                       
                                    </form>
                                    
                                    
                                </div>
                            </div>
                            <!-- /Login-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
           
        
    </body>
    @include('backend.common.scripts')
</html>
