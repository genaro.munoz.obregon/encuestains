@if(config('configSite.vertical'))
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
        <title> {{config('configSite.title_site')}} | @yield('title_page')</title>
        <meta content="" name="description" />
        <meta content="Genaro Muñoz Obregón" name="author" />
        <link rel="shortcut icon" href="{{asset('/')}}backend/assets_ver/images/favicon_z.webp">

        <meta name="csrf-token" content="{{ csrf_token() }}">
     
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>

        <link rel="apple-touch-icon" href="{{asset('/')}}backend/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('/')}}backend/images/ico/favicon.ico">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="{{asset('/')}}backend/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="{{asset('/')}}backend/vendors/css/charts/apexcharts.css">
        <link rel="stylesheet" type="text/css" href="{{asset('/')}}backend/vendors/css/extensions/toastr.min.css">
        <link rel="stylesheet" type="text/css" href="{{asset('/')}}backend/vendors/css/tables/datatable/dataTables.bootstrap5.min.css">
        <link rel="stylesheet" type="text/css" href="{{asset('/')}}backend/vendors/css/tables/datatable/responsive.bootstrap5.min.css">

        <link rel="stylesheet" type="text/css" href="{{asset('/')}}backend/vendors/css/animate/animate.min.css">
        <link rel="stylesheet" type="text/css" href="{{asset('/')}}backend/vendors/css/extensions/sweetalert2.min.css">
        
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="{{asset('/')}}backend/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="{{asset('/')}}backend/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="{{asset('/')}}backend/css/colors.css">
        <link rel="stylesheet" type="text/css" href="{{asset('/')}}backend/css/components.css">
        <link rel="stylesheet" type="text/css" href="{{asset('/')}}backend/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="{{asset('/')}}backend/css/themes/semi-dark-layout.css">

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="{{asset('/')}}backend/css/core/menu/menu-types/vertical-menu.css">
        <link rel="stylesheet" type="text/css" href="{{asset('/')}}backend/css/plugins/forms/form-validation.css">
        <link rel="stylesheet" type="text/css" href="{{asset('/')}}backend/css/pages/authentication.css">
        <link rel="stylesheet" type="text/css" href="{{asset('/')}}backend/css/plugins/charts/chart-apex.css">
        <link rel="stylesheet" type="text/css" href="{{asset('/')}}backend/css/plugins/extensions/ext-component-toastr.css">
        <link rel="stylesheet" type="text/css" href="{{asset('/')}}backend/css/pages/app-invoice-list.css">
        <!-- END: Page CSS-->

        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" type="text/css" href="{{asset('/')}}backend/assets/css/style.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <!-- END: Custom CSS-->

    </head>
@else


@endif
