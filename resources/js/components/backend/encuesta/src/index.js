export default {
    props: ["datos"],
     data() {
      return {
       nombre: '',
       instituto:  '',
       claridad: [],
       material: [],
       cumplio: [],
       utilidad: [],
       instalaciones: [],
       programacion: [],
       observaciones: '',
       asistir: '',
       errorshowmsj: [],
       showerrors: 0,
       showerrorobservaciones: 0,
       showerrornombre: 0,
       showerrorinstituto: 0,
       showerrorclaridad: 0,
       showerrormaterial: 0,
       showerrorcumplio: 0,
       showerroutilidad: 0,
       showerrorinstalaciones: 0,
       showerrorprogramacion: 0,
       showerrorasistir: 0,
       mostrar: 0,
     };
    },
    methods: {
        save(){
            if(this.validateform()){
                return false;
            }
            axios.post("/save", { 
                id: this.datos.id,
                nombre: this.nombre,
                instituto: this.instituto,
                claridad: JSON.stringify(this.claridad),
                material: JSON.stringify(this.material),
                cumplio: JSON.stringify(this.cumplio),
                utilidad: JSON.stringify(this.utilidad),
                instalaciones: JSON.stringify(this.instalaciones),
                programacion: JSON.stringify(this.programacion),
                observaciones: this.observaciones,
                asistir: this.asistir,
                datos: this.datos,
             }).then((res) => {
                if (res.data.status=="success") {
                    Swal.fire({
                        title: "Encuesta",
                        text: "Se almacenaron sus respuestas correctamente!",
                        icon: "success"
                      });
                    this.mostrar=1;
                }else{
                    Swal.fire({
                        title: "Encuesta realizada!",
                        text: "Ya ha presentado la encuesta con anterioridad!",
                        icon: "error"
                      });
                    location.href="/"
                }
            });
        },
        redirect(){
            window.location = "/";
        },
        validateform(){
            this.errorshowmsj= [];
            this.showerrors= 0;

            if(this.nombre===''){
                this.errorshowmsj.push("1");
                this.showerrornombre=1;
            }else{
                this.showerrornombre=0;
            }

            if(this.instituto===''){
                this.errorshowmsj.push("1");
                this.showerrorinstituto=1;
            }else{
                this.showerrorinstituto=0;
            }

            

            if(this.claridad.length===0){
                this.errorshowmsj.push("1");
                this.showerrorclaridad=1;
            }else{
                this.showerrorclaridad=0;
            }

            if(this.material.length===0){
                this.errorshowmsj.push("1");
                this.showerrormaterial=1;
            }else{
                this.showerrormaterial=0;
            }

            if(this.cumplio.length===0){
                this.errorshowmsj.push("1");
                this.showerrorcumplio=1;
            }else{
                this.showerrorcumplio=0;
            }

            if(this.utilidad.length===0){
                this.errorshowmsj.push("1");
                this.showerrorutilidad=1;
            }else{
                this.showerrorutilidad=0;
            }

            if(this.instalaciones.length===0){
                this.errorshowmsj.push("1");
                this.showerrorinstalaciones=1;
            }else{
                this.showerrorinstalaciones=0;
            }

            if(this.programacion.length===0){
                this.errorshowmsj.push("1");
                this.showerrorprogramacion=1;
            }else{
                this.showerrorprogramacion=0;
            }


            if(this.asistir===''){
                this.errorshowmsj.push("1");
                this.showerrorasistir=1;
            }else{
                this.showerrorasistir=0;
            }

            if (this.errorshowmsj.length) this.showerrors = 1;
            return this.showerrors;

        },
    },
    mounted() {
        
       
    },
   };