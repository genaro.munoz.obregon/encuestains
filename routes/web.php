<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuxiliarController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('backend/index');
});

/*Route::get('/encuesta', function () {
    return view('backend/encuesta');
});*/

Route::post('/valida-usuario',[AuxiliarController::class,'getUsuario'])->name('valida');
Route::get('/encuesta',[AuxiliarController::class,'getEncuesta'])->name('encuesta');
Route::post('/save',[AuxiliarController::class,'setEncuesta']);

