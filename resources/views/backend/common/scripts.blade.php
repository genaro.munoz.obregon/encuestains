    <!-- BEGIN: Vendor JS-->
    <script src="{{asset('/')}}backend/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('/')}}backend/vendors/js/forms/validation/jquery.validate.min.js"></script>
    <script src="{{asset('/')}}backend/vendors/js/charts/apexcharts.min.js"></script>
    <script src="{{asset('/')}}backend/vendors/js/extensions/toastr.min.js"></script>
    <script src="{{asset('/')}}backend/vendors/js/extensions/moment.min.js"></script>
    <script src="{{asset('/')}}backend/vendors/js/tables/datatable/jquery.dataTables.min.js"></script>
    <script src="{{asset('/')}}backend/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
    <script src="{{asset('/')}}backend/vendors/js/tables/datatable/dataTables.bootstrap5.min.js"></script>
    <script src="{{asset('/')}}backend/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
    <script src="{{asset('/')}}backend/vendors/js/tables/datatable/responsive.bootstrap5.js"></script>
    <script src="{{asset('/')}}backend/vendors/js/extensions/sweetalert2.all.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{asset('/')}}backend/js/core/app-menu.js"></script>
    <script src="{{asset('/')}}backend/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{asset('/')}}backend/js/scripts/pages/auth-login.js"></script>
    <script src="{{asset('/')}}backend/js/scripts/pages/dashboard-analytics.js"></script>
    <!-- END: Page JS-->
    <script src="{{asset('/')}}js/backend.js"></script>
    <script>
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>
