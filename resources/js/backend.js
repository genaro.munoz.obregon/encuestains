require('./bootstrap');

window.Vue = require("vue").default;
Vue.component('encuesta-component', require('./components/backend/encuesta/IndexComponent.vue').default);


import DataTable from "laravel-vue-datatable";
Vue.use(DataTable);


import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue);
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);


import VCalendar from 'v-calendar';

// Use v-calendar & v-date-picker components
Vue.use(VCalendar, {
    componentPrefix: 'vc',  // Use <vc-calendar /> instead of <v-calendar />
    // ...other defaults
});

import VueApexCharts from "vue-apexcharts";
Vue.use(VueApexCharts);

Vue.component("apexchart", VueApexCharts);

Vue.filter("capitalize", function (value) {
    if (!value)
        return '';
    value = value.toString();
    return value.charAt(0).toUpperCase() + value.slice(1);
});

Vue.filter("datefor", (value) => {
    var ndat = new Date(value);
    var days = [
        "Sun",
        "Mon",
        "Tue",
        "Wed",
        "Thu",
        "Fri",
        "Sat",
    ];
    var mont = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec",
    ];

    var numberday = ndat.getDate();
    var dayName = days[ndat.getDay()];
    var monthName = mont[ndat.getMonth()];
    var year = ndat.getFullYear();
    return numberday + ' ' + monthName + ' ' + year;
    //return dayjs(value).format("YYYY-MM-DD");
});

import VueMoment from 'vue-moment'
import moment from 'moment-timezone'

Vue.use(VueMoment, {
    moment,
})

import VueSweetalert2 from 'vue-sweetalert2';

// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css';

Vue.use(VueSweetalert2);

import Select2 from 'v-select2-component';
Vue.component('Select2', Select2);


import VueHorizontalCalendar from 'vue-horizontal-calendar';

let app = new Vue({
    el: '#app',
});
