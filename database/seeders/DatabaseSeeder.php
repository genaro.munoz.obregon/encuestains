<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'admin',
            'last_name'=>'Recruiter',
            'email' => 'admin@admin.com',
            'password' => bcrypt('prueba')
        ]);

        $role = Role::create(['name' => 'admin']);

        User::find(1)->assignRole($role);
        $role = Role::create(['name' => 'teacher']);
        $role = Role::create(['name' => 'academic']);
        $role = Role::create(['name' => 'executive']);

        $this->call([
            PermissionTableSeeder::class,
        ]);

    }
}
