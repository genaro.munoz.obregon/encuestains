<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiligenciaEncuestasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diligencia_encuestas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('idParticipante');
            $table->foreign('idParticipante')->references('id')->on('participantes');
            $table->string('pregunta1',255);
            $table->string('pregunta2',255);
            $table->string('pregunta3',45);
            $table->string('pregunta4',45);
            $table->string('pregunta5',45);
            $table->string('pregunta6',45);
            $table->string('pregunta7',45);
            $table->string('pregunta8',45);
            $table->text('pregunta9')->nullable();
            $table->string('pregunta10',10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diligencia_encuestas');
    }
}
