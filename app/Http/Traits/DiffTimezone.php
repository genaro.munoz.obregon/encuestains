<?php
namespace App\Http\Traits;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Throwable;
use DateTime;
use Carbon\Carbon;

trait DiffTimezone
{

    public function DiffTimeZoneCalculate($timeZone)
    {
        try{  
            //Obtener fecha con UTC
            $date = new Carbon();
            $date->setTimezone('UTC');
            //echo "Hour UTC ".$date."<br>";
            //Convierte la hora UTC al timezone del usuario logueado
            $newDate=new \DateTime($date, new \DateTimeZone($timeZone));
            //echo "Diferencia hora UTC y timezone user ".$newDate->format('P')."<br>";
            //Obtenemos la diferencia horaria en formato +05:00 o -04:00
            $timehourZone=$newDate->format('P');
            $searchsign = explode("+",$timehourZone);
            //Explode con signo = es para encontrar si el timezone es con signo =,si es asi el conteo sera igual a 2 de lo contrario sera 1
            if(count($searchsign)==2){
               $conver = str_replace("+","-",$timehourZone);
            }else {
               $conver = str_replace("-","+",$timehourZone);
            }
            return $timehourZone;
        } catch (Throwable $e) {            
            $message = $e->getMessage();
            Log::error('Trait calculate timezone for user zone: ' . $message);
            return false;
        }   
       
    }

    

};
