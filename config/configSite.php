<?php

return [
    'title_site' => 'XVIII Encuentro Científico',
    'logo_site' => '/img/logo-ins.svg',
    'icon_logo_site' => '/img/logo-ins.svg',
    'logo_site_light' => '/img/logo-ins.svg',
    'icon_logo_site_light' => '/img/logo-ins.svg',
    'vertical' => true,
    'menu_list' => [
        [
            'name' => 'Inicio',
            'route' => 'dashboard',
            'icon' => 'home',
            'type' => 'link',
            'role' => '1,2'
        ],
       
    ]
];
