@extends('backend.layouts.base')
@section('title_page','Dashboard')

@section('content')
<div id="app">
    <encuesta-component :datos="{{json_encode($data)}}"></encuesta-component>
</div>
@endsection
