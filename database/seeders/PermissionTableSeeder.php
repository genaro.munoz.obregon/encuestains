<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
        =========================== CONFIGURACIONES===================
        */
        // Query users
        Permission::create(['name' => 'admin-users']);
        Permission::create(['name' => 'users-List']);
        Permission::create(['name' => 'users-store']);
        Permission::create(['name' => 'users-getUser']);
        Permission::create(['name' => 'users-update']);
        Permission::create(['name' => 'users-delete']);

        //Roles
        Permission::create(['name' => 'admin-role']);
        Permission::create(['name' => 'role-List']);
        Permission::create(['name' => 'role-store']);
        Permission::create(['name' => 'role-getRol']);
        Permission::create(['name' => 'role-update']);
        Permission::create(['name' => 'role-delete']);

        //Teachers
        Permission::create(['name' => 'admin-teachers']);
        Permission::create(['name' => 'teachers-List']);
        Permission::create(['name' => 'teachers-store']);
        Permission::create(['name' => 'teachers-getTeacher']);
        Permission::create(['name' => 'teachers-update']);
        Permission::create(['name' => 'teachers-delete']);

        //Booking
        Permission::create(['name' => 'admin-booking']);
        Permission::create(['name' => 'booking-List']);
        Permission::create(['name' => 'booking-store']);
        Permission::create(['name' => 'booking-getBooking']);
        Permission::create(['name' => 'booking-update']);
        Permission::create(['name' => 'booking-delete']);


    }
}
