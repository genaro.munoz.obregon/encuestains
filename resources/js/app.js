require('./bootstrap');

window.Vue = require("vue").default;


//Registrando componentes frontend

import Vue from "vue";
import VueCountryCode from "vue-country-code-select";

Vue.use(VueCountryCode);

import Select2 from 'v-select2-component';
Vue.component('Select2', Select2);

import VueSweetalert2 from 'vue-sweetalert2';

// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css';

Vue.use(VueSweetalert2);

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue);
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);


import vueCountryRegionSelect from 'vue-country-region-select'
Vue.use(vueCountryRegionSelect)

import 'animate.css';

let app = new Vue({
    el:'#app',
});
