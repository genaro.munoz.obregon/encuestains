<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

use function React\Promise\reduce;

class checkStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->user()->status == 0) {
            return back()->withErrors(['email' => 'The user is not active',])->withInput(request(['email']));
        }
        return $next($request);
    }
}
