 const contactIcons = document.querySelectorAll('.contact-icon');
 let draggedIcon = null;
 
 contactIcons.forEach(icon => {
   icon.addEventListener('mousedown', (e) => {
     draggedIcon = icon;
     icon.style.cursor = 'grabbing';
     const offsetX = e.clientX - icon.getBoundingClientRect().left;
     const offsetY = e.clientY - icon.getBoundingClientRect().top;
 
     document.addEventListener('mousemove', onMouseMove);
     document.addEventListener('mouseup', onMouseUp);
 
     function onMouseMove(e) {
       if (!draggedIcon) return;
       const x = e.clientX - offsetX;
       const y = e.clientY - offsetY;
       draggedIcon.style.transform = `translate(${x}px, ${y}px)`;
     }
 
     function onMouseUp() {
       if (!draggedIcon) return;
       draggedIcon.style.cursor = 'grab';
       document.removeEventListener('mousemove', onMouseMove);
       document.removeEventListener('mouseup', onMouseUp);
       draggedIcon = null;
     }
   });
 });

 //Animación de entrada
 document.addEventListener('DOMContentLoaded', function () {
  const animatedElements = document.querySelectorAll('.animated-element');
  animatedElements.forEach((element, index) => {
    setTimeout(() => {
      element.classList.add('animate');
    }, index * 200); // Añadir un retraso para crear un efecto de uno detrás del otro
  });
});