<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DiligenciaEncuesta;
use App\Models\Participante;

class AuxiliarController extends Controller
{
    public function getUsuario(Request $request){
        $valida = Participante::where('cedula',"=",$request->documento)
            ->orWhere('correo','=',$request->documento)->first();

            if($valida){
                //return redirect()->route('encuesta')->with('code',base64_decode($request->documento));
                return redirect()->route('encuesta', ['code'=>encrypt($request->documento)]);
            }else{
                return back()->withErrors(['documento' => 'No se encuentra información en nuestros registros, por favor contacta con el administrador.',])
                ->withInput(request(['documento']));
            }
    }

    public function getEncuesta(Request $request){
        
        $valida = Participante::where('cedula',"=",decrypt($request->code))->where('presento','=',0)
            ->orWhere('correo','=',decrypt($request->code))->first();
        
        
        if($valida){
            //return redirect()->route('encuesta')->with('code',base64_decode($request->documento));
            return view('backend/encuesta')->with('data',$valida);
        }else{
            return back()->withErrors(['documento' => 'Ya ha presentado la encuesta con anterioridad, ir a consultar tu certificado <a href="https://directoriodeeventos.com/cert/SINGULAR_TECH_-_INS_22_AL_24_NOV">URL Certificado</a>.',])
            ->withInput(request(['documento']));
        }
     }

    public function setEncuesta(Request $request){

        $validaPresentacion = Participante::findOrFail($request->datos["id"]);

        if(!$validaPresentacion->presento){
            $new = new DiligenciaEncuesta();
            $new->idParticipante = $request->datos["id"];
            
            $new->pregunta1=str_replace('"','',$request->nombre);
            $new->pregunta2=str_replace('"','',$request->instituto);
            $new->pregunta3=str_replace('"','',$request->claridad);
            $new->pregunta4=str_replace('"','',$request->material);
            $new->pregunta5=str_replace('"','',$request->cumplio);
            $new->pregunta6=str_replace('"','',$request->utilidad);
            $new->pregunta7=str_replace('"','',$request->instalaciones);
            $new->pregunta8=str_replace('"','',$request->programacion);
            $new->pregunta9=str_replace('"','',$request->observaciones);
            $new->pregunta10=str_replace('"','',$request->asistir);
            $new->save();

            $actualiza = Participante::findOrFail($request->datos["id"]);
            $actualiza->presento= 1;
            $actualiza->save();

            $data = array(
                'message' => 'Encuesta realizada con exito',
                'status' =>'success',
                'title' => '',
                'code' => 200
            );
        }else{
            $data = array(
                'message' => 'Ya ha presentado la encuesta con éxito',
                'status' =>'failed',
                'title' => '',
                'code' => 200
            );
        }
        

        return response()->json($data,200);

    }
}
