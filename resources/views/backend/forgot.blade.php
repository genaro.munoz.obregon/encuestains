<!DOCTYPE html>
<html lang="en">

    @include('backend.common.head')

    <body>

        <!-- Begin page -->
        <div class="accountbg"></div>

        <div class="wrapper-page">
                <div class="card card-pages shadow-none">

                    <div class="card-body">
                        <div class="text-center m-t-0 m-b-15">
                            <a href="/" class="logo logo-admin"><img src="{{config('configSite.logo_site')}}" alt="" width="300px"></a>
                            <h5 class="font-18 text-center">Reset Password</h5>
                        </div>


                        <form class="form-horizontal m-t-30" action="{{ route('recovery') }}" method="POST">
                            @csrf
                            <div class="col-12">
                                <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        Enter your <b>email address</b> and instructions will be sent to you!
                                    </div>
                           </div>
                            <div class="form-group">
                                <div class="col-12{{$errors->has('email' ? 'is-invalid' : '')}}">
                                    <label>Email</label>
                                    <input class="form-control" value="{{old('email')}}" type="text" required="" id="email" name="email" placeholder="Email">


                                </div>
                            </div>

                            <div class="form-group text-center m-t-20">
                                <div class="col-12">
                                    <button class="btn btn-primary btn-block btn-lg waves-effect waves-light" type="submit">Send email</button>
                                    {!!$errors->first('email','<div class="row text-center pt-3 pl-3"><span class="alert alert-danger">:message</span></div>')!!}

                                    <div class="form-group row m-b-0">
                                        <div class="col-sm-12 mt-3">
                                            <a href="{{ route('login') }}" class="text-muted"><i class="fa fa-lock m-r-5"></i> Return</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        <!-- END wrapper -->



    </body>
    @include('backend.common.scripts')
</html>
