<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participantes', function (Blueprint $table) {
            $table->id();
            $table->string('nombre',40);
            $table->string('apellidos',60);
            $table->string('cedula',12)->nullable();
            $table->string('institucion',100)->nullable();
            $table->string('correo',80)->nullable();
            $table->string('tipoparticipacion',45)->nullable();
            $table->string('telefono',25);
            $table->boolean('presento')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participantes');
    }
}
